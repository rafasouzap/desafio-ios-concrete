//
//  RepositoryDTO.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import Foundation
import SwiftyJSON

class RepositoryListDTO: NSObject {
	
	var totalCount: Int!
	var items: [RepositoryItemDTO]!
	
	init(fromJson json: JSON!) {
		if (json.null == NSNull()) {
			return
		}
		
		totalCount = json["total_count"].intValue
		items = [RepositoryItemDTO]()
		
		let arrayItems = json["items"].arrayValue
		for item in arrayItems {
			let value = RepositoryItemDTO(fromJson: item)
			items.append(value)
		}
	}
}

class RepositoryItemDTO: NSObject {
	
	var id: Int!
	var name: String!
	var fullName: String!
	var htmlUrl: String!
	var pullRequesrUrl: String!
	var descriptions: String!
	var forks: Int!
	var stars: Int!
	var owner: UserDTO!
	
	init(fromJson json: JSON!) {
		if (json.null == NSNull()) {
			return
		}
		
		id = json["id"].intValue
		name = json["name"].stringValue
		fullName = json["full_name"].stringValue
		htmlUrl = json["html_url"].stringValue
		pullRequesrUrl = json["pulls_url"].stringValue
		descriptions = json["description"].stringValue
		forks = json["forks_count"].intValue
		stars = json["stargazers_count"].intValue
		
		let jsonOwner = json["owner"]
		if jsonOwner != JSON.null{
			owner = UserDTO(fromJson: jsonOwner)
		}
	}
	
}
