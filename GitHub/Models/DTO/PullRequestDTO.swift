//
//  PullRequestDTO.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import Foundation
import SwiftyJSON

class PullRequestListDTO: NSObject {
	
	var totalCount: Int!
	var items: [PullRequestItemDTO]!
	
	init(fromJson json: JSON!) {
		if (json.null == NSNull()) {
			return
		}
		
//		totalCount = json["total_count"].intValue
		items = [PullRequestItemDTO]()
		
		let arrayItems = json.arrayValue
		for item in arrayItems {
			let value = PullRequestItemDTO(fromJson: item)
			items.append(value)
		}
	}
}

class PullRequestItemDTO: NSObject {
	
	var id: Int!
	var htmlUrl: String!
	var state: String!
	var title: String!
	var body: String!
	var createdAt: String!
	var user: UserDTO!
	
	init(fromJson json: JSON!) {
		if (json.null == NSNull()) {
			return
		}
		
		id = json["id"].intValue
		htmlUrl = json["html_url"].stringValue
		state = json["state"].stringValue
		title = json["title"].stringValue
		body = json["body"].stringValue
		createdAt = json["created_at"].stringValue
		
		let jsonUser = json["user"]
		if jsonUser != JSON.null{
			user = UserDTO(fromJson: jsonUser)
		}
	}
	
}
