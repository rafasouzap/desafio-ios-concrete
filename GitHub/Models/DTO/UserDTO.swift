//
//  UserDTO.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserDTO: NSObject {
	
	var id: Int!
	var login: String!
	var avatarUrl: String!
	var htmlUrl: String!
	
	init(fromJson json: JSON!) {
		if (json.null == NSNull()) {
			return
		}
		
		id = json["id"].intValue
		login = json["login"].stringValue
		htmlUrl = json["html_url"].stringValue
		avatarUrl = json["avatar_url"].stringValue
	}
	
}
