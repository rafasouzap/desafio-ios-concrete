//
//  GitHubService.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GitHubService: NSObject {

	static func getPopularRepositories(language:(String), page:(Int),
	                                   completionHandler:@escaping (RepositoryListDTO?) -> Void,
	                                   errorHandler:@escaping (String) -> Void) {

		Alamofire.request(ServiceLocations.searchRepositoriesUrl,
		                  method: .get,
		                  parameters: [
							Constants.queryTag: String(format: Constants.queryByLanguage, language),
							Constants.sortByTag: Constants.sortByStars,
							Constants.pageTag: page],
		                  encoding: URLEncoding.default,
		                  headers: nil)
			.responseJSON { response in

				print(response.request as Any)
				
				switch (response.result) {
					case .failure(_):
						errorHandler(Constants.connectionDownMessage)
					case .success(_):
						completionHandler(RepositoryListDTO(fromJson: JSON(response.result.value!)))
				}
		}
	}
	
	static func getPullRequests(repository:(String), 
								completionHandler:@escaping (PullRequestListDTO?) -> Void,
								errorHandler:@escaping (String) -> Void) {
		
		Alamofire.request(String(format: ServiceLocations.pullRequestsUrl, repository),
		                  method: .get,
		                  parameters: nil,
		                  encoding: URLEncoding.default,
		                  headers: nil)
			.responseJSON { response in
				
				print(response.request as Any)
				
				switch (response.result) {
					case .failure(_):
						errorHandler(Constants.connectionDownMessage)
					case .success(_):
						completionHandler(PullRequestListDTO(fromJson: JSON(response.result.value!)))
				}
		}
	}
	
}
