//
//  Constants.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import Foundation

class Constants {
	
	// Messages
	static let connectionDownMessage = "Não foi possível estabelecer conexão com o servidor"
	
	// Parameters tags
	static let pageTag = "page"
	static let sortByTag = "sort"
	static let queryTag = "q"
	
	// Parameters values
	static let sortByStars = "stars"
	static let sortByCreated = "created"
	static let queryByLanguage = "language:%@"
	static let queryByIssues = "repo:%@+is:%@+type:pr"
	static let openStatus = "open"
	static let javaLanguage = "Java"
	
}
