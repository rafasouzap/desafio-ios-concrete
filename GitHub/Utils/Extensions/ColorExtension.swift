//
//  ColorExtension.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit

extension UIColor {
	static func navigationBarColor() -> UIColor {
		return UIColor(red: 41.0/255.0, green: 47.0/255.0, blue: 53.0/255.0, alpha: 1.0)
	}
	static func backgroundColor() -> UIColor {
		return UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
	}
}
