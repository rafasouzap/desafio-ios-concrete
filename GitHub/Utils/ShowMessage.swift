//
//  ShowMessage.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/28/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import SwiftMessages

class ShowMessage {
	
	static func Warning (message: String, title: String) -> Void {
		
		var config = SwiftMessages.Config()
		config.presentationStyle = .top
		config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
		config.duration = .seconds(seconds: 5)
		config.dimMode = .gray(interactive: true)
		config.preferredStatusBarStyle = UIStatusBarStyle.lightContent
		
		let view = MessageView.viewFromNib(layout: .MessageView)
		view.configureTheme(.error)
		view.configureDropShadow()
		view.configureContent(title: title, body: message)
		view.button?.isHidden = true
		
		SwiftMessages.show(config: config, view: view)
	}
	
}
