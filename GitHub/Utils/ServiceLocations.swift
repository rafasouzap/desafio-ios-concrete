//
//  ServiceLocations.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import Foundation

class ServiceLocations {
	
	// Repositories API
	static let searchRepositoriesUrl = "https://api.github.com/search/repositories"
	
	// Pull Requests API
	static let pullRequestsUrl = "https://api.github.com/repos/%@/pulls"

}
