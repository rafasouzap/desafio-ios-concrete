//
//  PullRequestTableViewController.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/27/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit
import AlamofireImage
import ARSLineProgress

class PullRequestTableViewController: UITableViewController {
	
	var repository: RepositoryItemDTO!
	var pullRequests: [PullRequestItemDTO]!
	var pageCount: Int = 1
	var pullOpenedCount: Int = 0
	var pullClosedCount: Int = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.backgroundColor = UIColor.backgroundColor()
		
		navigationController?.navigationBar.barTintColor = UIColor.navigationBarColor()
		
		self.pullRequests = [PullRequestItemDTO]()
		self.loadData()
	}
	
	func loadData() -> Void {
		
		ARSLineProgress.show()
		
		GitHubService.getPullRequests(repository: repository.fullName, completionHandler: ({(response) in
		
			let pulls = response!.items
			self.pullRequests.append(contentsOf: pulls!)
			self.tableView.reloadData()
			
			ARSLineProgress.hide()
			
		}), errorHandler: {(error) in
			ShowMessage.Warning(message: error, title: "Ops!")
			ARSLineProgress.hide()
		})
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.pullRequests.count
	}
	
	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 30
	}
	
	override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
		let header = view as! UITableViewHeaderFooterView
		header.textLabel?.font = UIFont(name: "Helvetica-Bold", size: 12)
		header.textLabel?.textColor = UIColor.darkGray
		header.textLabel?.textAlignment = NSTextAlignment.center
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		//return String(format: "%d opened / %d closed", self.pullOpenedCount, self.pullClosedCount)
		return String(format: "%d opened", self.pullRequests.count)
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let pull = self.pullRequests[indexPath.row]
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell", for: indexPath) as! PullRequestViewCell
		
		cell.pullRequestDescription.text = pull.body
		cell.pullRequestTitle.text = pull.title
		cell.pullRequestUsername.text = pull.user.login
		
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
		let date = formatter.date(from: pull.createdAt)
		
		formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
		cell.pullRequestDate.text = formatter.string(from: date!)
		
		let avatarUrl = NSURL(string: pull.user.avatarUrl)
		cell.pullRequestImageAvatar.af_setImage(withURL: avatarUrl as! URL)
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let url = NSURL(string: self.pullRequests[indexPath.row].htmlUrl)
		UIApplication.shared.openURL(url! as URL)
	}
}
