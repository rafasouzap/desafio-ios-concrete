//
//  RepositoryViewCell.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit

class RepositoryViewCell: UITableViewCell {
	@IBOutlet weak var repositoryName: UILabel!
	@IBOutlet weak var repositoryDescription: UILabel!
	@IBOutlet weak var repositoryForks: UILabel!
	@IBOutlet weak var repositoryStars: UILabel!
	@IBOutlet weak var repositoryUsername: UILabel!
	@IBOutlet weak var repositoryFullname: UILabel!
	@IBOutlet weak var repositoryImageAvatar: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		self.repositoryImageAvatar.layer.cornerRadius = self.repositoryImageAvatar.frame.size.width / 2
		self.repositoryImageAvatar.clipsToBounds = true
	}
}
