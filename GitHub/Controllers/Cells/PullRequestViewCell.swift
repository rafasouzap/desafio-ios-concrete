//
//  PullRequestViewCell.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/27/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit

class PullRequestViewCell: UITableViewCell {
	@IBOutlet weak var pullRequestTitle: UILabel!
	@IBOutlet weak var pullRequestDescription: UILabel!
	@IBOutlet weak var pullRequestUsername: UILabel!
	@IBOutlet weak var pullRequestDate: UILabel!
	@IBOutlet weak var pullRequestImageAvatar: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		self.pullRequestImageAvatar.layer.cornerRadius = self.pullRequestImageAvatar.frame.size.width / 2
		self.pullRequestImageAvatar.clipsToBounds = true
	}
}
