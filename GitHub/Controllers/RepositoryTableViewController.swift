//
//  RepositoryTableViewController.swift
//  GitHub
//
//  Created by Rafael de Paula on 3/26/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit
import AlamofireImage
import ARSLineProgress

class RepositoryTableViewController: UITableViewController {
	
	var popularRepos: [RepositoryItemDTO]!
	var pageCount: Int = 1
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.backgroundColor = UIColor.backgroundColor()
		
		navigationController?.navigationBar.barTintColor = UIColor.navigationBarColor()
		
		let imageView = UIImageView(image: #imageLiteral(resourceName: "GitHubLogo"))
		navigationItem.titleView = imageView
		
		self.tableView.infiniteScrollIndicatorStyle = .gray
		
		self.popularRepos = [RepositoryItemDTO]()
		self.loadData(pageNumber: 1)

		self.tableView.addInfiniteScroll { (scrollView) -> Void in
			let tableView = scrollView 
			self.pageCount = self.pageCount + 1
			self.loadData(pageNumber: self.pageCount)
			
			tableView.finishInfiniteScroll()
		}
	}
	
	func loadData(pageNumber: Int) -> Void {
		
		ARSLineProgress.show()
		
		GitHubService.getPopularRepositories(language: "Java", page: pageNumber, completionHandler: ({(response) in
			
			let repos = response!.items
			self.popularRepos.append(contentsOf: repos!)
			self.tableView.reloadData()
			
			ARSLineProgress.hide()
			
		}), errorHandler: {(error) in
			ShowMessage.Warning(message: error, title: "Ops!")
			ARSLineProgress.hide()
		})
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.popularRepos.count
	}
	
	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 30
	}
	
	override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
		let header = view as! UITableViewHeaderFooterView
		header.textLabel?.font = UIFont(name: "Helvetica-Bold", size: 12)
		header.textLabel?.textColor = UIColor.darkGray
		header.textLabel?.textAlignment = NSTextAlignment.center
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return "Java Popular Repositories"
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let repository = self.popularRepos[indexPath.row]
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoryViewCell
		cell.repositoryName.text = repository.name
		cell.repositoryDescription.text = repository.descriptions
		cell.repositoryStars.text = String(repository.stars)
		cell.repositoryForks.text = String(repository.forks)
		cell.repositoryUsername.text = repository.owner.login
		cell.repositoryFullname.text = repository.fullName
		
		let avatarUrl = NSURL(string: repository.owner.avatarUrl)
		cell.repositoryImageAvatar.af_setImage(withURL: avatarUrl as! URL)
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selectedRepository = self.popularRepos[indexPath.row]
		self.performSegue(withIdentifier: "PullRequestSegue", sender: selectedRepository)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "PullRequestSegue") {
			if let indexPath = tableView.indexPathForSelectedRow {
				let repository: RepositoryItemDTO = self.popularRepos[indexPath.row]
				let controller = segue.destination as! PullRequestTableViewController
				controller.repository = repository
			}
		}
	}
}
